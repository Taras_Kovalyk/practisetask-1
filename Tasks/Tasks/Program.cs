﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Tasks
{
    class Program
    {   
        static void Task1()
        {
            Console.WriteLine("Task #1. Vectors");
            Console.WriteLine(new string('*', 50));
            Console.WriteLine();

            Vector vectorA = new Vector(new int[] { -4, 5, 1, 3, 4 });
            Vector vectorB = new Vector(new int[] { 1, 15, 7, -1, 6 });
            Vector vectorC;
            Vector vectorD;
            Vector vectorE = new Vector(new int[] { 1, 15, 7, -1, 6 });
            
            Console.WriteLine("VectorA: " + vectorA);
            Console.WriteLine("VectorB: " + vectorB);
            Console.WriteLine(new string('-', 50));
          
            vectorD = vectorA + vectorB;
            Console.WriteLine($"VectorD = ({vectorA} + {vectorB}) = {vectorD} ");
           

            vectorD = vectorD - (vectorA + vectorB);
            Console.WriteLine($"VectorD = ({vectorA} - {vectorB}) = {vectorD} ");
          
            vectorC = -vectorA * 2;
            Console.WriteLine($"VectorC = -({-vectorA}) * 2 = {vectorC} ");
            Console.WriteLine(new string('-', 50));

            Console.WriteLine("Comparing");
            Console.WriteLine($"({vectorB}) = ({vectorE})? - { vectorB == vectorE }");
            Console.WriteLine($"({vectorB}) = ({vectorA})? - { vectorB == vectorA }");
            Console.WriteLine($"({vectorB}) != ({vectorE})? - { vectorB != vectorE }");
            Console.WriteLine($"({vectorB}) != ({vectorA})? - { vectorB != vectorA }");
             Console.WriteLine($"HashCode of ({vectorB}) is {vectorB.GetHashCode()}; HashCode of ({vectorA}) is {vectorA.GetHashCode()}; ");
             Console.WriteLine($"HashCode of ({vectorB}) is {vectorB.GetHashCode()}; HashCode of ({vectorE}) is {vectorE.GetHashCode()}; ");
             
             Console.WriteLine();
            Console.WriteLine(new string('*', 50));
              Console.WriteLine();
     }

        static void Task3()
        {
            Console.WriteLine("Task #3. Polynomials");
            Console.WriteLine(new string('*', 50));
            Console.WriteLine();
           
            var polynom1 = new Polynomial(3, 3, 0, 1, -8);
            var polynom2 = new Polynomial(3, 0, 2, 1, 0);
            var polynom3 = new Polynomial(4, -6, 0, -1, 0, 5);

            Console.WriteLine(polynom1);
            Console.WriteLine(polynom2);
            Console.WriteLine(polynom3);
            Console.WriteLine(new string('-', 50));

            var polynom4 = polynom1 + polynom2;
            Console.WriteLine($"({ polynom1 }) + ({ polynom2 }) = { polynom4 }");
            polynom4 = polynom1 - polynom2;

            Console.WriteLine($"({ polynom1 }) - ({ polynom2 }) = { polynom4 }");                     
            Console.WriteLine($"-({ -polynom3 }) * ({ polynom2 }) = { -polynom3 * polynom2 }");
            Console.WriteLine(new string('-', 50));

            Console.WriteLine("Polynom value of given varible:");
            Console.WriteLine(new string('-', 50));
            Console.WriteLine($"{polynom3}, lets put -2 instead of x:  calculated value = {polynom3.CalculatePolynomialValue(-2)}" );
          
            Console.WriteLine();
            Console.WriteLine(new string('*', 50));
            Console.WriteLine();
     }

        static void Task2()
        {
            Console.WriteLine("Task #2. Rectangles");
            Console.WriteLine(new string('*', 50));
            Console.WriteLine();
           
           var rectangle1 = new Rectangle(0, 0, 15, 12);
           var rectangle2 = new Rectangle(2, 2, 11, 17);        
           var intersection = rectangle1.InterSect(rectangle2);
          
            Console.WriteLine($"Start point (0,0), width: 15, height: 12  => Rectangle1: {rectangle1.BottomLeft.ToString()}, {rectangle1.BottomRight.ToString()}, {rectangle1.TopLeft.ToString()}, {rectangle1.TopRight.ToString()} ");
            Console.WriteLine($"Start point (2,2), width: 11, height: 17  => Rectangle2: {rectangle2.BottomLeft.ToString()}, {rectangle2.BottomRight.ToString()}, {rectangle2.TopLeft.ToString()}, {rectangle2.TopRight.ToString()} ");
            Console.WriteLine($"Intersection of rectangle1 and rectangle2 => Intersection rectangle: {intersection.BottomLeft.ToString()}, {intersection.BottomRight.ToString()}, {intersection.TopLeft.ToString()}, {intersection.TopRight.ToString()} ");
          
            rectangle1.ChangeLocationByXY(100, 100);
          
            Console.WriteLine($"Shift location of rectangle1: By X:100, ByY:100");            
            Console.WriteLine($"After shifting: {rectangle1.BottomLeft.ToString()}, {rectangle1.BottomRight.ToString()}, {rectangle1.TopLeft.ToString()}, {rectangle1.TopRight.ToString()} ");
            Console.WriteLine($"Width and height: {rectangle1.Width}, {rectangle2.Height}");           
            
            
             Console.WriteLine($"Set new size for rectangle1: width:35, height:46"); 
             rectangle1.SetNewSize(35, 46);
             Console.WriteLine($"After resizing: {rectangle1.BottomLeft.ToString()}, {rectangle1.BottomRight.ToString()}, {rectangle1.TopLeft.ToString()}, {rectangle1.TopRight.ToString()} ");
                               
          
            Console.WriteLine();
            Console.WriteLine(new string('*', 50));
            Console.WriteLine();
        }

        static void Task4()
        {
             Console.WriteLine("Task #4. Matrix");
            Console.WriteLine(new string('*', 50));
            Console.WriteLine();
           
           var matrix1 = new Matrix(new int[2, 2] { {1, 2}, {0, -4} });
           var matrix2 = new Matrix(new int[2, 2] { {0, 7}, {10, -14} });
           
            Console.WriteLine($"matrix1:\n {matrix1.ToString()}");
            Console.WriteLine($"matrix2:\n {matrix2.ToString()}");

            var matrix3 = matrix1 + matrix2;
            Console.WriteLine($"matrix3 = matrix1 + matrix2:\n {matrix3.ToString()}");

            matrix3 = matrix1 - matrix2;
            Console.WriteLine($"matrix3 = matrix1 - matrix2:\n {matrix3.ToString()}");

            matrix3 = matrix1 - -matrix2;
            Console.WriteLine($"matrix3 = matrix1 - -(matrix2):\n {matrix3.ToString()}");
          
            Console.WriteLine();
            Console.WriteLine(new string('*', 50));
            Console.WriteLine();
        }

        static void Main(string[] args)
        {         
             Task1();
             Task2();
             Task3();
             Task4();

            Console.ReadKey();
        }
    }
}
